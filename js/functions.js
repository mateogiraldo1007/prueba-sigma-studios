window.onload = function(){
    var tab1 = document.getElementById('tab1');
    var tab2 = document.getElementById('tab2');
    var content1 = document.getElementById('content1');
    var content2 = document.getElementById('content2');
    var contenedorPpal = document.getElementById('principal');

    tab1.addEventListener('click', function(){
        content1.style.setProperty('display', 'block');
        content2.style.setProperty('display', 'none');
        contenedorPpal.style.setProperty('height', '');     
        tab1.classList.remove('unClicked');        
        tab2.classList.add('unClicked');
        tab1.classList.add('tabClicked');        
        tab2.classList.remove('tabClicked');
    });
    tab2.addEventListener('click', function(){
        content1.style.setProperty('display', 'none');
        content2.style.setProperty('display', 'block');
        contenedorPpal.style.setProperty('height', '100vh');
        tab1.classList.remove('tabClicked');        
        tab2.classList.add('tabClicked');
        tab1.classList.add('unClicked');        
        tab2.classList.remove('unClicked');
    });
}